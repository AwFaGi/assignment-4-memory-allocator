#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#define line "[========================================================================================]"
#define TEST_HEAP_SIZE (REGION_MIN_SIZE*5)
#define MY_MAP_ANONYMOUS 0x20

void wrap_sentence(char* message){
    printf("\n%s\n", message);
}

void print_error(char* message){
    fprintf(stderr, "%s", message);
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

static bool test_1(){
    void* heap = heap_init(TEST_HEAP_SIZE);

    if (heap == NULL){
        print_error("Error on create heap");
        return false;
    }

    wrap_sentence("Initial:");
    debug_heap(stdout, heap);

    void* block = _malloc(23);

    if (block == NULL){
        print_error("Error on malloc block");
        return false;
    }

    wrap_sentence("After _malloc(23):");
    debug_heap(stdout, heap);

    _free(block);

    wrap_sentence("After _free:");
    debug_heap(stdout, heap);

    void* block2 = _malloc(4093);

    if (block2 == NULL){
        print_error("Error on malloc block2");
        return false;
    }

    wrap_sentence("After _malloc(4093):");
    debug_heap(stdout, heap);

    _free(block2);

    wrap_sentence("After _free:");
    debug_heap(stdout, heap);

    munmap(heap, size_from_capacity((block_capacity){.bytes = TEST_HEAP_SIZE}).bytes);

    return true;
}

static bool test_2(){

    void* heap = heap_init(TEST_HEAP_SIZE);

    if (heap == NULL){
        print_error("Error on create heap");
        return false;
    }

    wrap_sentence("Initial:");
    debug_heap(stdout, heap);

    void* block1 = _malloc(1234);

    if (block1 == NULL){
        print_error("Error on malloc block1");
        return false;
    }

    wrap_sentence("After _malloc block1:");
    debug_heap(stdout, heap);

    void* block2 = _malloc(4093);

    if (block2 == NULL){
        print_error("Error on malloc block2");
        return false;
    }

    wrap_sentence("After _malloc block2:");
    debug_heap(stdout, heap);

    void* block3 = _malloc(4093);

    if (block3 == NULL){
        print_error("Error on malloc block3");
        return false;
    }

    wrap_sentence("After _malloc block3:");
    debug_heap(stdout, heap);

    _free(block2);

    wrap_sentence("After _free block 2:");
    debug_heap(stdout, heap);

    _free(block1);
    _free(block3);

    munmap(heap, size_from_capacity((block_capacity){.bytes = TEST_HEAP_SIZE}).bytes);

    return true;

}

static bool test_3(){

    void* heap = heap_init(TEST_HEAP_SIZE);

    if (heap == NULL){
        print_error("Error on create heap");
        return false;
    }

    wrap_sentence("Initial:");
    debug_heap(stdout, heap);

    void* block1 = _malloc(1234);

    if (block1 == NULL){
        print_error("Error on malloc block1");
        return false;
    }

    wrap_sentence("After _malloc block1:");
    debug_heap(stdout, heap);

    void* block2 = _malloc(4093);

    if (block2 == NULL){
        print_error("Error on malloc block2");
        return false;
    }

    wrap_sentence("After _malloc block2:");
    debug_heap(stdout, heap);

    void* block3 = _malloc(4093);

    if (block3 == NULL){
        print_error("Error on malloc block3");
        return false;
    }

    wrap_sentence("After _malloc block3:");
    debug_heap(stdout, heap);

    void* block4 = _malloc(4093);

    if (block4 == NULL){
        print_error("Error on malloc block4");
        return false;
    }

    wrap_sentence("After _malloc block4:");
    debug_heap(stdout, heap);

    _free(block2);

    wrap_sentence("After _free block 2:");
    debug_heap(stdout, heap);

    _free(block3);

    wrap_sentence("After _free block 3:");
    debug_heap(stdout, heap);

    _free(block1);
    _free(block4);

    munmap(heap, size_from_capacity((block_capacity){.bytes = TEST_HEAP_SIZE}).bytes);

    return true;

}

static bool test_4(){

    void* heap = heap_init(TEST_HEAP_SIZE);

    if (heap == NULL){
        print_error("Error on create heap");
        return false;
    }

    wrap_sentence("Initial:");
    debug_heap(stdout, heap);

    void* block1 = _malloc(TEST_HEAP_SIZE + 512);

    wrap_sentence("After _malloc block1:");
    debug_heap(stdout, heap);

    if (block1 == NULL){
        print_error("Error on malloc block1");
        return false;
    }

    if (block_get_header( block1)->capacity.bytes < TEST_HEAP_SIZE + 512){
        print_error("Capacity of block1 is too small\n");
        return false;
    }

    _free(block1);

    debug_heap(stdout, heap);

    munmap(heap, size_from_capacity((block_capacity){.bytes = 45039}).bytes);

    return true;

}

static bool test_5(){

    void* heap = heap_init(1);

    if (heap == NULL){
        print_error("Error on create heap");
        return false;
    }

    wrap_sentence("Initial:");
    debug_heap(stdout, heap);

    void* block1 = _malloc(20000);

    wrap_sentence("After _malloc block1:");
    debug_heap(stdout, heap);

    if (block1 == NULL){
        print_error("Error on malloc block1");
        return false;
    }

    struct block_header* header = block_get_header(block1)->next;
    void* offset = header->contents + header->capacity.bytes;
    void* my = mmap(offset, 4096, PROT_READ | PROT_WRITE, MAP_PRIVATE | MY_MAP_ANONYMOUS, -1, 0);

    if (my == MAP_FAILED){
        print_error("Error on mmap");
        return false;
    }

    printf("\nSozrali memori na %p\n", my);

    void* block2 = _malloc(10000);

    wrap_sentence("After _malloc block2:");
    debug_heap(stdout, heap);

    if (block2 == NULL){
        print_error("Error on malloc block2");
        return false;
    }

    _free(block1);
    _free(block2);

    munmap(heap, size_from_capacity((block_capacity){.bytes = TEST_HEAP_SIZE}).bytes);
    munmap(offset, 4096);

    return true;

}

int main(){

    wrap_sentence(line);
    printf("Test 1");
    bool res_1 = test_1();

    wrap_sentence(line);
    printf("Test 2");
    bool res_2 = test_2();

    wrap_sentence(line);
    printf("Test 3");
    bool res_3 = test_3();

    wrap_sentence(line);
    printf("Test 4");
    bool res_4 = test_4();

    wrap_sentence(line);
    printf("Test 5");
    bool res_5 = test_5();


    wrap_sentence(line);
    if (res_1){
        printf("Test 1 passed\n");
    } else{
        printf("Test 1 failed\n");
    }

    if (res_2){
        printf("Test 2 passed\n");
    } else{
        printf("Test 2 failed\n");
    }

    if (res_3){
        printf("Test 3 passed\n");
    } else{
        printf("Test 3 failed\n");
    }

    if (res_4){
        printf("Test 4 passed\n");
    } else{
        printf("Test 4 failed\n");
    }

    if (res_5){
        printf("Test 5 passed\n");
    } else{
        printf("Test 5 failed\n");
    }

    wrap_sentence(line);
    printf("Passed %d test out of 5", res_1 + res_2 + res_3 + res_4 + res_5);
    wrap_sentence("");

    return 0;
}